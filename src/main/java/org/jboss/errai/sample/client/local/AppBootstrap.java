package org.jboss.errai.sample.client.local;

import com.google.gwt.user.client.ui.*;
import org.jboss.errai.ioc.client.api.EntryPoint;
import org.jboss.errai.sample.client.local.view.services.ApplicationConfig;
import org.jboss.errai.sample.client.local.view.services.ResourcesInjector;
import org.jboss.errai.ui.nav.client.local.Navigation;
import org.jboss.errai.ui.nav.client.local.TransitionAnchor;
import org.jboss.errai.ui.shared.api.annotations.DataField;
import org.jboss.errai.ui.shared.api.annotations.Templated;
import javax.annotation.PostConstruct;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

@EntryPoint
@Templated("#root")
public class AppBootstrap extends Composite {

    public static final String CLASS_TAB_ACTIVE = "is-active";

    @Inject
    private Navigation navigation;

    @Inject
    private ResourcesInjector resourcesInjector;

    @Inject
    private ApplicationConfig config;

    @Inject @DataField
    private TransitionAnchor<HelloPage> helloTab;

    @Inject @DataField
    private TransitionAnchor<TestPage> homeTab;

    @Inject
    @DataField
    private SimplePanel pageContent;

    @PostConstruct
    public void init() {
        pageContent.add(navigation.getContentPanel());
        RootPanel.get().add(this);
    }

    private void initUI(@Observes PageLoaded event) {
        // MDL on dynamic sites has to be initialized manually (http://www.getmdl.io/started/index.html#dynamic)
        resourcesInjector.initMaterialDesignLite();
    }

    private void selectBookmarkedTab(@Observes PageLoaded event) {
        Anchor currentTab = event.getPageName().equals(HelloPage.PAGE_NAME) ? helloTab : homeTab;
        currentTab.getElement().addClassName(CLASS_TAB_ACTIVE);
    }
}