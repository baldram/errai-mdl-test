/*
 * Copyright 2016 Marcin Szałomski
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.jboss.errai.sample.client.local;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import org.jboss.errai.ui.nav.client.local.Navigation;
import org.jboss.errai.ui.nav.client.local.PageShown;
import org.jboss.errai.ui.nav.client.local.spi.PageNode;

import javax.enterprise.event.Event;
import javax.inject.Inject;

public abstract class BasePage extends Composite {

    @Inject
    private Event<PageLoaded> pageLoadedEvent;

    @Inject
    private Navigation navigation;

    @PageShown
    private void onPageLoaded() {
        PageNode<IsWidget> currentPage = navigation.getCurrentPage();
        if (currentPage != null) {
            pageLoadedEvent.fire(new PageLoaded(currentPage.getURL()));
        }
    }
}
