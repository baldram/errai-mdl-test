/*
 * Copyright 2016 Marcin Szałomski
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.jboss.errai.sample.client.local;

import com.google.common.collect.Iterables;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.TextBox;
import org.jboss.errai.ui.nav.client.local.Page;
import org.jboss.errai.ui.shared.api.annotations.DataField;
import org.jboss.errai.ui.shared.api.annotations.EventHandler;
import org.jboss.errai.ui.shared.api.annotations.Templated;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

import static org.jboss.errai.sample.client.local.HelloPage.PAGE_NAME;

@Templated
@Page(path=PAGE_NAME)
public class HelloPage extends BasePage {

    public static final String PAGE_NAME = "hello";
    public static final String JAVA_8_TEST = "Java 8";

    @Inject
    @DataField
    private TextBox name;

    @Inject
    @DataField
    private Button hello;

    @EventHandler("hello")
    private void onHello(ClickEvent e) {
        Window.alert("Hello " + name.getText() + "!"
                + " It's a " + getItemFromList(JAVA_8_TEST) + "!");
    }

    private String getItemFromList(final String item) {
        final List<String> items = Arrays.asList("java", JAVA_8_TEST, "test");
        return Iterables.tryFind(items, someTestItem -> someTestItem.equals(item)).orNull();
    }
}
