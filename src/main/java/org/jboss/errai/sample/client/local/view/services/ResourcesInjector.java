/*
 * Copyright 2016 Marcin Szałomski
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.jboss.errai.sample.client.local.view.services;

import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.HeadElement;
import com.google.gwt.dom.client.LinkElement;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ResourcesInjector {

    public static final String HEAD_MISSING_NOTIFICATION = "The <head> element is missing!";
    public static final String TAG_HEAD = "head";
    public static final String STYLESHEET_TYPE = "text/css";
    public static final String STYLESHEET_RELATIONSHIP = "stylesheet";

    private HeadElement head;

    public void injectScripts(String... uris) {
        for (String uri : uris) {
            ScriptInjector.fromUrl(uri).setWindow(ScriptInjector.TOP_WINDOW).inject();
        }
    }

    public void injectStylesheets(String... uris) {
        for (String uri : uris) {
            injectResourceCss(uri);
        }
    }

    public final native void initMaterialDesignLite() /*-{
        // Needs to be called to finish MDL initialization
        $wnd.componentHandler.upgradeDom();
    }-*/;

    private void injectResourceCss(String uri) {
        LinkElement link = Document.get().createLinkElement();
        link.setType(STYLESHEET_TYPE);
        link.setRel(STYLESHEET_RELATIONSHIP);
        link.setHref(uri);
        getHead().appendChild(link);
    }

    private HeadElement getHead() {
        if (head == null) {
            Element domElement = Document.get().getElementsByTagName(TAG_HEAD).getItem(0);
            assert domElement != null : HEAD_MISSING_NOTIFICATION;
            head = HeadElement.as(domElement);
        }
        return head;
    }

}
