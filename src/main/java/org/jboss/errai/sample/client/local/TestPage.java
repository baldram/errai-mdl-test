package org.jboss.errai.sample.client.local;

import org.jboss.errai.ui.nav.client.local.DefaultPage;
import org.jboss.errai.ui.nav.client.local.Page;

import org.jboss.errai.ui.shared.api.annotations.Templated;
import static org.jboss.errai.sample.client.local.TestPage.PAGE_NAME;

@Templated("#pageContent")
@Page(role = DefaultPage.class, path=PAGE_NAME)
public class TestPage extends BasePage {
    public static final String PAGE_NAME = "home";
}
